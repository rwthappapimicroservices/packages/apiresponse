using System;

namespace PIT.Labs.ApiResponseLib
{
    public static class ApiResponseFactory
    {
        public const string STATUS_OK = "ok";
        public const string STATUS_ERROR = "error";
        public const string STATUS_TOKEN_INVALID = "token invalid";

        public static ApiResponse<T> Create<T>(T data)
        {
            return new ApiResponse<T>(data, ApiResponseFactory.STATUS_OK, 0, false);
        }

        public static ApiResponse<T> CreateError<T>(string statusText, int statusCode, T data = default(T))
        {
            return new ApiResponse<T>(data, statusText, statusCode, true);
        }

        public static ApiResponse<T> CreateErrorFromException<T>(Exception ex)
        {
            if (ex is CustomException)
            {
                var customException = (CustomException)ex;
                return ApiResponseFactory.CreateError<T>(customException.Message, customException.ErrorCode);
            }
            else
            {
                return ApiResponseFactory.CreateErrorUnexpectedError<T>();
            }

        }

        public static ApiResponse<T> CreateErrorUnexpectedError<T>(T data = default(T))
        {
            return ApiResponseFactory.CreateError<T>(ApiResponseFactory.STATUS_ERROR, -1, data);
        }

        public static ApiResponse<T> CreateErrorTokenInvalid<T>(string scope = null, T data = default(T))
        {
            string error = $"Token invalid.";
            if (scope != null)
            {
                error = $"Token invalid: Token expired or scope '{ scope }' is missing.";
            }

            return ApiResponseFactory.CreateError<T>(error, 1, data);
        }

        public static ApiResponse<T> CreateErrorTokenGroupMissing<T>(string[] missingGroups = null, T data = default(T))
        {
            string error = $"Token invalid: Permission groups are missing.";
            if (missingGroups != null)
            {
                error = $"Token invalid: Permission group(s) '{ String.Join(",", missingGroups) }' missing.";
            }

            return ApiResponseFactory.CreateError<T>(error, 2, data);
        }

    }
}
